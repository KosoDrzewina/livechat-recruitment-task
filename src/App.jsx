import React from "react";
import Mountain from "./assets/images/mountain.jpg";

// COMPONENTS:
import ChatWindow from "./components/Chat";

const App = () => {
  return (
    <div className="app-container">
      <img
        alt="Mountain"
        src={Mountain}
        className="app-container--background"
      />
      <ChatWindow />
    </div>
  );
};

export default App;

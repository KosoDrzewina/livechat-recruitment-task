import { v4 } from "uuid";

import { chatsIds, messages, users } from "./data";

// CONSTANTS:
const anonymousUser = {
  first_name: "Anonymous",
  last_name: "",
};
const statusCodes = {
  created: 201,
  success: 200,
  error: 404,
};
const errorResponse = {
  msg: "Something went wrong",
  code: "random_error",
};

const mockApi = (method, path, body) => {
  return new Promise((resolve, reject) => {
    // Gets random value from range between 300 - 1000
    const randomDelayTime = (Math.floor(Math.random() * 10) + 3) * 100;

    /**
     * Get messages for selected Chat
     * @param chatId
     * @returns {*[]}
     * @private
     */
    const _getChatsMessages = (chatId) => {
      return messages.filter((message) => message.chat_uuid === chatId);
    };

    /**
     * Returns count of all messages in given Chat
     * @param chatId
     * @returns {number}
     * @private
     */
    const _calculateMessagesCount = (chatId) => {
      const chatMessages = messages.filter((chat) => chat.chat_uuid === chatId);

      return chatMessages.length;
    };

    /**
     * Get only unique id's of users from given messages array
     * @param messages
     * @returns {*[]}
     * @private
     */
    const _getUniqueUserIds = (messages) => {
      const userIds = [];
      messages.map((message) => userIds.push(message.author_uuid));
      return [...new Set(userIds)];
    };

    /**
     * Get array of usernames for given Chat's messages
     * @param messagesList
     * @returns {Array}
     * @private
     */
    const _getChatUserNames = (messagesList) => {
      const userNames = [];
      const uniqueUserIds = _getUniqueUserIds(messagesList);
      uniqueUserIds.map((userId) => {
        let userAdded = false;
        users.map((user) => {
          if (userId === user.user_uuid) {
            userAdded = true;
            userNames.push(`${user.first_name} ${user.last_name}`);
          }
          return user;
        });
        if (!userAdded) userNames.push("Anonymous");
        return userId;
      });
      return userNames;
    };

    /**
     * Get list of objects of all users from given messages array
     * @param messagesList
     * @returns {*[]}
     * @private
     */
    const _getChatsUsers = (messagesList) => {
      const userIds = _getUniqueUserIds(messagesList);
      return users.filter((user) => userIds.includes(user.user_uuid));
    };

    /**
     * Assign users objects to matching messages by user_uuid
     * @param messages
     * @param users
     * @returns {*[]}
     * @private
     */
    const _assignUsersToMessages = (messages, users) => {
      const messagesWithUsers = messages;
      messagesWithUsers.map((message) => {
        users.map((userInChat) => {
          if (message.author_uuid === userInChat.user_uuid)
            message.user = userInChat;
          return userInChat;
        });
        if (!message.user) message.user = anonymousUser;
        return message;
      });
      return messagesWithUsers;
    };

    /**
     * Get list of collected messages with assigned users
     * @returns {*[]}
     * @private
     */
    const _getMessagesWithUsers = () => {
      const messagesInChat = _getChatsMessages(body.chatId);
      const usersInChat = _getChatsUsers(messagesInChat);
      return _assignUsersToMessages(messagesInChat, usersInChat);
    };

    /**
     * Get list of chats with assigned usernames and counted messages
     * @private
     */
    const _getChatsList = () => {
      const chatsList = chatsIds;
      chatsList.map((chat) => {
        const messagesList = _getChatsMessages(chat.chat_uuid);
        chat.users = _getChatUserNames(messagesList);
        chat.messages_count = _calculateMessagesCount(chat.chat_uuid);
        return chat;
      });
      return chatsList;
    };

    if (method === "GET") {
      switch (true) {
        case path === "chats":
          setTimeout(() => {
            resolve({
              data: _getChatsList(),
              status: statusCodes.success,
            });
          }, randomDelayTime);
          break;

        case path === "messages":
          setTimeout(() => {
            resolve({
              data: _getMessagesWithUsers(),
              status: statusCodes.success,
            });
          }, randomDelayTime);
          break;

        default:
          setTimeout(
            () =>
              reject({
                data: errorResponse,
                status: statusCodes.error,
              }),
            randomDelayTime
          );
          break;
      }
    }

    if (method === "POST") {
      switch (true) {
        case path === "message":
          messages.push({
            message_uuid: v4(),
            chat_uuid: body.chatId,
            author_uuid: "4e84e23c-785c-4ac0-bbd9-cb05139cd037",
            text: body.messageText,
          });
          setTimeout(() => {
            resolve({
              data: _getMessagesWithUsers(),
              status: statusCodes.created,
            });
          }, randomDelayTime);
          break;

        default:
          setTimeout(
            () =>
              reject({
                data: errorResponse,
                status: statusCodes.error,
              }),
            randomDelayTime
          );
          break;
      }
    }
  });
};

export default mockApi;

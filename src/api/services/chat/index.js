import MockedResponse from "../../mockAPI";

/**
 * Get list of all chats
 * @returns {*[]}
 */
export const getChatsList = async () => {
  try {
    const response = await MockedResponse("GET", "chats");
    if (response.status === 200) {
      return response.data;
    }
  } catch (response) {
    alert(response.data.msg);
  }
};

/**
 * Get list of messages with assigned user objects
 * @param chatId
 * @returns {*[]}
 */
export const getMessagesWithUsers = async (chatId) => {
  try {
    const response = await MockedResponse("GET", "messages", { chatId });
    if (response.status === 200) {
      return response.data;
    }
  } catch (response) {
    alert(response.data.msg);
  }
};

/**
 * Add new message to selected Chat
 * @param chatId
 * @param messageText
 * @returns {Promise<*>}
 */
export const addMessageToChat = async (chatId, messageText) => {
  try {
    const response = await MockedResponse("POST", "message", {
      chatId,
      messageText,
    });
    if (response.status === 201) {
      return response.data;
    }
  } catch (response) {
    alert(response.data.msg);
  }
};

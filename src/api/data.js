export const chatsIds = [
  { chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d" },
  { chat_uuid: "5c9c3261-61cd-4084-9910-aa07b4b589b9" },
  { chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152" },
];

export let messages = [
  {
    message_uuid: "c0b941a2-e7aa-4184-9f77-32fe18c8a8e7",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "4beafea9-bb74-4ce8-8f8b-fcde99964368",
    text: "Hello",
  },
  {
    message_uuid: "47ceafa8-4d13-4908-a8af-b2fdd5fe4d34",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "e79eae5a-28f3-4248-b6bf-5b93d615477e",
    text: "Hi",
  },
  {
    message_uuid: "b185fce8-3057-46bf-9b09-f7896dc36c43",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "4beafea9-bb74-4ce8-8f8b-fcde99964368",
    text: "Dawaj na ring",
  },
  {
    message_uuid: "7399a4eb-e3e0-40ca-b270-c343ac0871d2",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "34c7d342-a264-4846-9739-e8de32baaaeb",
    text: "Bibibibibibi",
  },
  {
    message_uuid: "527e92bd-492f-4add-ab76-f91e1938d662",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "e79eae5a-28f3-4248-b6bf-5b93d615477e",
    text: "Wolał bym nie",
  },
  {
    message_uuid: "0c9787a3-fe69-408f-b942-7be43e65cef2",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "4beafea9-bb74-4ce8-8f8b-fcde99964368",
    text: "Zaraz Cię zniszcze",
  },
  {
    message_uuid: "93663c38-d5f0-4906-b819-7a4e006e83ca",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "34c7d342-a264-4846-9739-e8de32baaaeb",
    text: "Bobobobobobo",
  },
  {
    message_uuid: "34983da2-a48a-492f-92f1-c609c0facd84",
    chat_uuid: "855a8f65-df2f-4cfc-ad95-10baf4baa152",
    author_uuid: "4beafea9-bb74-4ce8-8f8b-fcde99964368",
    text: "Jezu nie",
  },

  {
    message_uuid: "414041ed-52ee-43cb-b44a-8bf54aa6e9c0",
    chat_uuid: "5c9c3261-61cd-4084-9910-aa07b4b589b9",
    author_uuid: "15e2cc2e-82e8-44ba-a909-149b53757e5d",
    text: "Hello",
  },
  {
    message_uuid: "db74744d-e983-45d6-845f-de4b7ba4a145",
    chat_uuid: "5c9c3261-61cd-4084-9910-aa07b4b589b9",
    author_uuid: "9ca32fc1-07eb-4cc3-ac1b-dfd0245e4860",
    text: "Bo jesteś Ty",
  },
  {
    message_uuid: "036ddf6b-991d-4a29-bfb7-b16dff4f261a",
    chat_uuid: "5c9c3261-61cd-4084-9910-aa07b4b589b9",
    author_uuid: "b2495bd9-288a-4ffc-bdfe-91859fbd8059",
    text: "O nie znowu to robi",
  },
  {
    message_uuid: "f5e673fe-9811-4020-b01f-17950817d8ae",
    chat_uuid: "5c9c3261-61cd-4084-9910-aa07b4b589b9",
    author_uuid: "9ca32fc1-07eb-4cc3-ac1b-dfd0245e4860",
    text: "Cóż więcej mógł bym chcieć",
  },

  {
    message_uuid: "3b777c22-5f7d-4552-8294-7363c68f6682",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "ba405586-3a7f-484b-b5c0-5d1cf5cd9c0e",
    text: "Hi!",
  },
  {
    message_uuid: "5af483bc-7c9e-4c5f-936e-1201c4bb239a",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "9bf2612e-442c-41f9-977d-17a9a83bc649",
    text: "Serdelki",
  },
  {
    message_uuid: "f1f43866-8099-499b-a063-e329475f61cb",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "9bf2612e-442c-41f9-977d-17a9a83bc649",
    text: "Chleb",
  },
  {
    message_uuid: "a57f571f-dc94-41e5-aaf3-17c90de019d9",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "785c2399-2e28-4aa5-85a5-6a968cc0c8e4",
    text:
      "Marcepan sarcepan saracen sracenMarcepan sarcepapan saracen sracenMarcepan sarcepapan saracen sracenMarcepapan sarcepapan saracen sracenMarcepapan sarcepapan saracen sracenMarcepapan sarcepapan saracen sracenMarcepapan sarcepapan saracen sracenMarcepan sarcepapan saracen sracenMarcepan sarcepan saracen sracenMarcepan sarcepan saracen sracenMarcepan sarcepan saracen sracen",
  },
  {
    message_uuid: "900901d8-104d-4e01-bd8d-e7c61ff2c26d",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "ba405586-3a7f-484b-b5c0-5d1cf5cd9c0e",
    text: "Kotlety",
  },
  {
    message_uuid: "6f9b57ca-931b-47cd-b0a7-495ada7f6fbf",
    chat_uuid: "66019eab-d2cb-4bb6-a873-aa8acf2d116d",
    author_uuid: "9bf2612e-442c-41f9-977d-17a9a83bc649",
    text: "Kotlety",
  },
];

export const users = [
  {
    user_uuid: "ba405586-3a7f-484b-b5c0-5d1cf5cd9c0e",
    first_name: "John",
    last_name: "Doe",
  },
  {
    user_uuid: "9bf2612e-442c-41f9-977d-17a9a83bc649",
    first_name: "Will",
    last_name: "Smith",
  },
  {
    user_uuid: "15e2cc2e-82e8-44ba-a909-149b53757e5d",
    first_name: "John",
    last_name: "Dee",
  },
  {
    user_uuid: "b2495bd9-288a-4ffc-bdfe-91859fbd8059",
    first_name: "Bat",
    last_name: "Man",
  },
  {
    user_uuid: "9ca32fc1-07eb-4cc3-ac1b-dfd0245e4860",
    first_name: "Krzysztof",
    last_name: "Krawczyk",
  },
  {
    user_uuid: "4beafea9-bb74-4ce8-8f8b-fcde99964368",
    first_name: "Mariusz",
    last_name: "Pudzianowski",
  },
  {
    user_uuid: "e79eae5a-28f3-4248-b6bf-5b93d615477e",
    first_name: "Brat",
    last_name: "Mariusza",
  },
  {
    user_uuid: "34c7d342-a264-4846-9739-e8de32baaaeb",
    first_name: "Robert",
    last_name: "Makłowicz",
  },
  {
    user_uuid: "4e84e23c-785c-4ac0-bbd9-cb05139cd037",
    first_name: "Mateusz",
    last_name: "Wajchaprzełóż",
  },
];

import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const Loader = ({ loaderClass, loaderSize }) => {
  return (
    <div data-testid="loader-container" className={loaderClass}>
      <FontAwesomeIcon
        data-testid="loader-icon"
        spin
        size={loaderSize}
        icon={faSpinner}
      />
    </div>
  );
};

Loader.propTypes = {
  loaderClass: PropTypes.string,
  loaderSize: PropTypes.string,
};

export default Loader;

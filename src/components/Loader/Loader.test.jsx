import React from "react";
import { render } from "@testing-library/react";

import Loader from "../Loader";

describe("Loader component", () => {
  test("Check if Loader renders size correctly", async () => {
    const { getByTestId } = render(<Loader loaderSize="lg" />);
    const loader = getByTestId("loader-icon");

    expect(loader.classList.contains("fa-lg")).toBeTruthy();
  });

  test("Check if Loader Container renders customClasses correctly", async () => {
    const { getByTestId } = render(<Loader loaderClass="test-class" />);
    const loaderContainer = getByTestId("loader-container");

    expect(loaderContainer.classList.contains("test-class")).toBeTruthy();
  });
});

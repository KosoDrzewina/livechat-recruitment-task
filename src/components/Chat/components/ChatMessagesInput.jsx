import React, { useState } from "react";
import PropTypes from "prop-types";

// API:
import { addMessageToChat } from "../../../api/services/chat";

// COMPONENTS:
import ButtonLoading from "../../ButtonLoading";
import Input from "../../Input";

const ChatMessagesInput = ({ messagesEnd, chatItemId, setMessagesList }) => {
  const [inputValue, setInputValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  /**
   * Handle sending request and displaying new messages
   * @returns {Promise<void>}
   */
  const handleSubmitMessage = async () => {
    setIsLoading(true);
    const response = await addMessageToChat(chatItemId, inputValue);
    setIsLoading(false);
    setInputValue("");
    setMessagesList(response);
    scrollToBottom();
  };

  /**
   * Handle storing input value in state
   * @param e
   */
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  /**
   * Scroll to the bottom of messages container
   */
  const scrollToBottom = () => {
    messagesEnd.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div className="tabs-item__input">
      <Input
        value={inputValue}
        onEnter={handleSubmitMessage}
        handleChange={handleInputChange}
        placeholder="Type your message..."
      />

      <ButtonLoading
        label="Submit"
        isLoading={isLoading}
        handleClick={handleSubmitMessage}
      />
    </div>
  );
};

ChatMessagesInput.propTypes = {
  messagesEnd: PropTypes.any,
  chatItemId: PropTypes.string,
  setMessagesList: PropTypes.func,
};

export default ChatMessagesInput;

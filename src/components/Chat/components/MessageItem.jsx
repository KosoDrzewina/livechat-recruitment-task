import React from "react";
import PropTypes from "prop-types";

// COMPONENTS:
import Badge from "../../Badge";

const MessageItem = ({ message }) => {
  /**
   * Get initials of selected user
   * @param user
   * @returns {string}
   */
  const getUserInitials = (user) => {
    const firstNameInitial = user.first_name.substring(0, 1) || "";
    const lastNameInitial = user.last_name.substring(0, 1) || "";

    return `${firstNameInitial}${lastNameInitial}`;
  };

  return (
    <div className="message-item" key={message.message_uuid}>
      <div className="message-item__avatar">
        <Badge
          customClass="avatar__badge"
          color="success"
          badgeText={getUserInitials(message.user)}
        />
      </div>
      <div className="message-item__body">
        <div className="body__user-info">
          {message.user.first_name} {message?.user.last_name}
        </div>
        <div className="body__content">{message.text}</div>
      </div>
    </div>
  );
};

MessageItem.propTypes = {
  message: PropTypes.shape({
    message_uuid: PropTypes.string,
    chat_uuid: PropTypes.string,
    author_uuid: PropTypes.string,
    text: PropTypes.string,
  }),
};

export default MessageItem;

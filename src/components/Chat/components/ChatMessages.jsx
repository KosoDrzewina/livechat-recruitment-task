import React, { useState, useEffect, useCallback, useRef } from "react";
import PropTypes from "prop-types";

// API:
import { getMessagesWithUsers } from "../../../api/services/chat";

// COMPONENTS:
import MessageItem from "./MessageItem";
import MessageInput from "./ChatMessagesInput";
import Loader from "../../Loader";

const ChatMessages = ({ chatsItem, activeTab }) => {
  const [messagesList, setMessagesList] = useState([]);
  const [isFetching, setIsFetching] = useState(true);

  const messagesEnd = useRef(null);

  /**
   * Get list of messages
   */
  const handleGetMessages = useCallback(async (chatId) => {
    const response = await getMessagesWithUsers(chatId);
    handleSetMessageList(response);
    setIsFetching(false);
  }, []);

  useEffect(() => {
    if (activeTab === chatsItem.chat_uuid)
      handleGetMessages(chatsItem.chat_uuid);
  }, [chatsItem, activeTab, handleGetMessages]);

  /**
   * Handle setting messagesList to state
   * @param messageList
   */
  const handleSetMessageList = (messageList) => {
    setMessagesList(messageList);
  };

  return (
    <>
      {activeTab === chatsItem.chat_uuid && (
        <div className="tabs-item">
          <div className="tabs-item__messages">
            {isFetching && (
              <Loader loaderClass="messages__loader" loaderSize="lg" />
            )}

            {!isFetching &&
              messagesList.map((message) => (
                <MessageItem key={message.message_uuid} message={message} />
              ))}
            <div ref={messagesEnd} />
          </div>

          <MessageInput
            setMessagesList={handleSetMessageList}
            chatItemId={chatsItem.chat_uuid}
            messagesEnd={messagesEnd}
          />
        </div>
      )}
    </>
  );
};

ChatMessages.propTypes = {
  chatItem: PropTypes.shape({
    messages_count: PropTypes.number,
    chat_uuid: PropTypes.string,
    users: PropTypes.array,
  }),
  activeTab: PropTypes.string,
};

export default ChatMessages;

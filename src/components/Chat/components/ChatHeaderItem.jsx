import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ReactTooltip from "react-tooltip";

// COMPONENTS:
import Badge from "../../Badge";

const ChatHeaderItem = ({ chatItem, activeTab, changeTab }) => {
  const [isUnread, setIsUnread] = useState(true);

  useEffect(() => {
    if (activeTab === chatItem.chat_uuid) setIsUnread(false);
  }, [activeTab, chatItem.chat_uuid]);

  /**
   * Concat first name of users and trim string
   * @param users
   * @returns {string}
   */
  const prepareChatHeader = (users) => {
    const firstNames = [];
    users.map((user) => firstNames.push(user.replace(/ .*/, "")));

    return `${firstNames.join(", ").substring(0, 10)} ...`;
  };

  /**
   * Change tab to selected Chat
   */
  const handleChangeTab = () => {
    changeTab(chatItem.chat_uuid);
  };

  /**
   * Get usernames separated by comma
   * @param users
   * @returns {string}
   */
  const prepareChatTooltip = (users) => {
    const userNames = [];
    users.map((user) => userNames.push(user));

    return userNames.join(", ");
  };

  return (
    <>
      <li
        onClick={handleChangeTab}
        className={`navigation-tabs__item ${
          activeTab === chatItem.chat_uuid && "navigation-tabs__item--active"
        }`}
      >
        <span
          data-tip={prepareChatTooltip(chatItem.users)}
          id={`chat-header-${chatItem.chat_uuid}`}
          className="tab-item__link"
        >
          {prepareChatHeader(chatItem.users)}{" "}
          {isUnread && (
            <Badge
              customClass="link__badge"
              color="warning"
              badgeText={chatItem.messages_count.toString()}
            />
          )}
        </span>
      </li>
      <ReactTooltip place="top" type="dark" effect="solid" />
    </>
  );
};

ChatHeaderItem.propTypes = {
  chatItem: PropTypes.shape({
    messages_count: PropTypes.number,
    chat_uuid: PropTypes.string,
    users: PropTypes.array,
  }),
  activeTab: PropTypes.string,
  changeTab: PropTypes.func,
};

export default ChatHeaderItem;

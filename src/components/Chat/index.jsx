import React, { useState, useEffect, useCallback } from "react";

// API:
import { getChatsList } from "../../api/services/chat";

// COMPONENTS:
import ChatHeaderItem from "./components/ChatHeaderItem";
import ChatMessages from "./components/ChatMessages";
import Loader from "../Loader";

const ChatWindow = () => {
  const [activeTab, setActiveTab] = useState("");
  const [isFetching, setIsFetching] = useState(true);
  const [chatsList, setChatsList] = useState([]);

  useEffect(() => {
    handleGetChats();
  });

  useEffect(() => {
    if (chatsList.length > 0) {
      setActiveTab(chatsList[0].chat_uuid);
    }
  }, [chatsList]);

  /**
   * Handle getting list of chats and storing it in state
   */
  const handleGetChats = useCallback(async () => {
    const response = await getChatsList();
    setChatsList(response);
    setIsFetching(false);
  }, []);

  /**
   * Handle changing to selected Chat
   * @param tab
   */
  const changeTab = (tab) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  return (
    <>
      <div className="container">
        <div className="chat-window-container">
          <div className="chat-window">
            <div className="chat-window__navs">
              <ul
                className={`navigation-tabs ${
                  isFetching && "navigation-tabs--loading"
                }`}
              >
                {isFetching && (
                  <Loader
                    loaderClass="navigation-tabs__loader"
                    loaderSize="lg"
                  />
                )}
                {!isFetching &&
                  chatsList.map((chat) => (
                    <ChatHeaderItem
                      key={chat.chat_uuid}
                      chatItem={chat}
                      activeTab={activeTab}
                      changeTab={changeTab}
                    />
                  ))}
              </ul>
            </div>

            <div className="chat-window__tabs">
              {!isFetching &&
                chatsList.map((chat) => (
                  <ChatMessages
                    key={chat.chat_uuid}
                    chatsItem={chat}
                    getChats={handleGetChats}
                    activeTab={activeTab}
                  />
                ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ChatWindow;

import React from "react";
import PropTypes from "prop-types";

const Badge = ({ badgeText, color, customClass }) => {
  return (
    <span
      data-testid="badge"
      className={`badge badge--${color} ${customClass}`}
    >
      {badgeText}
    </span>
  );
};

Badge.propTypes = {
  badgeText: PropTypes.string,
  color: PropTypes.string,
  customClass: PropTypes.string,
};

export default Badge;

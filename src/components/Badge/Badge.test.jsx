import React from "react";
import { render } from "@testing-library/react";

import Badge from "../Badge";

describe("Badge component", () => {
  test("Check if Badge component renders label correctly", async () => {
    const { getByText } = render(<Badge badgeText="TS" />);
    expect(getByText("TS")).toBeTruthy();
  });

  test("Check if Badge renders color correctly", async () => {
    const { getByTestId } = render(<Badge color="success" badgeText="TS" />);
    const badge = getByTestId("badge");

    expect(badge.classList.contains("badge--success")).toBeTruthy();
  });

  test("Check if Badge renders customClasses correctly", async () => {
    const { getByTestId } = render(
      <Badge customClass="test-class" color="success" badgeText="TS" />
    );
    const badge = getByTestId("badge");

    expect(badge.classList.contains("test-class")).toBeTruthy();
  });
});

import React from "react";
import { render } from "@testing-library/react";
import ButtonLoading from "../ButtonLoading";

describe("ButtonLoading component", () => {
  test("Check if ButtonLoading label renders correctly", async () => {
    const { getByText } = render(
      <ButtonLoading id="test" isLoading={false} label="Label" />
    );
    expect(getByText("Label")).toBeTruthy();
  });

  test("Check if ButtonLoading is disabled while loading", async () => {
    const { getByTestId } = render(
      <ButtonLoading isLoading={true} label="Label" />
    );
    const button = getByTestId("btn-loading");

    expect(button.classList.contains("btn--disabled")).toBeTruthy();
    expect(button.disabled).toBeTruthy();
  });

  test("Check if ButtonLoading display loading icon", async () => {
    const { getByTestId } = render(<ButtonLoading isLoading label="Label" />);

    const button = getByTestId("btn-loading");
    const icon = getByTestId("btn-loading__loading");

    expect(button.disabled).toBeTruthy();
    expect(icon).toBeTruthy();
  });
});

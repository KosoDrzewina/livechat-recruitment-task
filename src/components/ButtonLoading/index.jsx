import React from "react";
import PropTypes from "prop-types";

// COMPONENTS:
import Loader from "../Loader";

const ButtonLoading = ({ handleClick, label, isLoading }) => {
  return (
    <button
      data-testid="btn-loading"
      onClick={handleClick}
      disabled={isLoading}
      className={`btn ${isLoading && "btn--disabled"}`}
    >
      <span>
        {!isLoading && label}
        {isLoading && (
          <span data-testid="btn-loading__loading">
            <Loader />
          </span>
        )}
      </span>
    </button>
  );
};

ButtonLoading.propTypes = {
  handleClick: PropTypes.func,
  label: PropTypes.string,
  isLoading: PropTypes.bool,
};

export default ButtonLoading;

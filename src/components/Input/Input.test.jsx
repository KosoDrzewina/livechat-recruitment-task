import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Input from "../Input";

const setup = () => {
  const utils = render(<Input placeholder="Placeholder" />);
  const input = utils.getByPlaceholderText("Placeholder");
  return {
    input,
    ...utils,
  };
};

describe("Input component", () => {
  test("Check if Input component renders placeholder correctly", async () => {
    const { getByPlaceholderText } = render(
      <Input placeholder="Placeholder" />
    );
    expect(getByPlaceholderText("Placeholder")).toBeTruthy();
  });

  test("Check if Input component handle changing value correctly", async () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: "Test value" } });
    expect(input.value).toBe("Test value");
  });
});

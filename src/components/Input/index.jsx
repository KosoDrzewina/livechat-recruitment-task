import React from "react";
import PropTypes from "prop-types";

const Input = ({ value, handleChange, placeholder, onEnter }) => {
  /**
   * Execute onEnter action when user presses "enter" key
   * @param e
   */
  const handleEnterClick = (e) => {
    if (e.key === "Enter") {
      onEnter();
    }
  };
  return (
    <input
      value={value}
      onKeyDown={handleEnterClick}
      onChange={handleChange}
      className="input"
      placeholder={placeholder}
    />
  );
};

Input.propTypes = {
  value: PropTypes.string,
  handleChange: PropTypes.func,
  placeholder: PropTypes.string,
};

export default Input;

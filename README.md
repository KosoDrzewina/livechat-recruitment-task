# LiveChat recruitment task

Simple chat application

### Installing

- Clone repository with `git clone git@bitbucket.org:KosoDrzewina/livechat-recruitment-task.git`
- Open terminal in fetched folder
- Install dependencies with `npm i`

### Running the tests

- Run unit test with `npm run test`

### Preview

- Run development server with `npm start`
